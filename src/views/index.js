import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import {
    Button,
    InputItem
} from 'antd-mobile'

@inject('store')
@observer
class Index extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: ''
        }
    }

    changeName = (e) => {
        this.setState({
            name: e
        });
    }

    setName = () => {
        this.props.store.setName(this.state.name)
    }

    render() {
        return (
            <div>
                <p style={{textAlign:'center'}}>
                  我的名字 {
                    this.props.store.name
                  }
                </p>
                <InputItem value={this.state.name} type='primary' onChange={e => this.changeName(e)}>姓名</InputItem>
                <Button onClick={(e) => {this.setName(e)}}>修改</Button>
            </div>
        )
    }
}

export default Index

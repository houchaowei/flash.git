import React from 'react';
import ReactDOM from 'react-dom';
import {
    HashRouter as Router,
    // BrowserRouter as Router,
    // Route,
    // Link,
    // Redirect,
    // withRouter
} from "react-router-dom";
import 'antd-mobile/dist/antd-mobile.css';
import fastclick from 'fastclick'
// build config
import * as serviceWorker from './serviceWorker';
// router config
import routerConfig from '@/router/config'
import {RouterView} from "@/router";
// mobx
import { Provider } from 'mobx-react'
import CommonState from './mobx/CommonState'
const store = new CommonState();

fastclick.attach(document.body, {
  tapDelay: 20,
  tapTimeout: 100
})

class App extends React.Component {

    render() {
        return (
            <Provider {...this.props}>
                <Router >
                    <div className="app">
                        <RouterView routes={routerConfig}></RouterView>
                    </div>
                </Router>
            </Provider>
        );
    }
}

ReactDOM.render(<App store={store}/>, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
